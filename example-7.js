import {cleanConsole, createAll} from './data';

const companies = createAll();

cleanConsole(7, companies);
console.log('---- EXAMPLE 7 part 1 --- ', p1GetCompanyName(7));
console.log('---- EXAMPLE 7 part 2 --- ', p2DeleteCompany(6));
console.log('---- EXAMPLE 7 part 3 --- ', p3PatchCompany(5, {name: 'Cocacola'}));
console.log('---- EXAMPLE 7 part 4 --- ', p4CreateUser(4, {
  firstName: 'Juan',
  lastName: 'Delgado',
  age: 35,
  car: true,
}));
console.log('---- EXAMPLE 7 part 5 --- ', p5PutCompany(3, {name: 'nike'}));
console.log('---- EXAMPLE 7 part 6 --- ', p6DeleteUser(2, 0));
console.log('---- EXAMPLE 7 part 7 --- ', p7PatchUser(1, 0, {firstName: 'rigoberto'}));
console.log('---- EXAMPLE 7 part 8 --- ', p8PutUser(0, 0, {firstName: 'rigoberto'}));
console.log('---- EXAMPLE 7 part 9 --- ', p9MoveUser(1, 2, 0));
console.log('---- EXAMPLE 7 --- ', companies);

function getIndex(id, list) {
  return list.findIndex((element) => element.id === id);
}

function check(neo, old) {
  return typeof neo === 'undefined' ? old: neo;
}

function p1GetCompanyName(id) {
  const index = getIndex(id, companies);
  if (index < 0) return 'not fount company';
  return companies[index].name;
}

function p2DeleteCompany(id) {
  const index = getIndex(id, companies);
  if (index < 0) return 'not fount company';
  companies.splice(index, 1);
  // console.log('companies : ', companies);
  return 'delete company';
}

function p3PatchCompany(id, {name, isOpen, usersLength, newId}) {
  const index = getIndex(id, companies);
  if (index < 0) return 'not fount company';

  const company = companies[index];
  companies.splice(index, 1, {
    ...company,
    name: check(name, company.name),
    isOpen: check(isOpen, company.isOpen),
    usersLength: check(usersLength, company.usersLength), // 🙄
    id: check(newId, id), // 🙄
  });
  // console.log('companies : ', companies);
  return 'patch company';
}

function p5PutCompany(id, {name, isOpen, usersLength, newId}) {
  const index = getIndex(id, companies);
  if (index < 0) return 'not fount company';

  const company = companies[index];
  companies.splice(index, 1, {
    name: check(name, ''),
    isOpen: check(isOpen, ''),
    usersLength: check(usersLength, NaN), // 🙄
    id: check(newId, id), // 🙄
    users: company.users,
  });

  // console.log('companies : ', companies);
  return 'put company';
}

function p4CreateUser(idCompany, {firstName, lastName, car, age}) {
  const index = getIndex(idCompany, companies);
  if (index < 0) return 'not fount company';

  companies[index].users.push({
    fistName: check(firstName, ''),
    lastName: check(lastName, ''),
    age: check(age, NaN),
    car: check(car, false),
    id: Date.now() + Math.floor(Math.random() * 1000), // ! can't use generators 🙄
  });

  // console.log('companies : ', companies);
  return 'add User';
}

function p6DeleteUser(idCompany, idUser) {
  const indexCompany = getIndex(idCompany, companies);
  if (indexCompany < 0) return 'not fount company';

  const indexUser = getIndex(idUser, companies[indexCompany].users);
  if (indexUser < 0) return 'not fount user';

  companies[indexCompany].users.splice(indexUser, 1);

  // console.log('companies : ', companies);
  return 'delete User';
}

function p7PatchUser(idCompany, idUser, {firstName, lastName, car, age, newId}) {
  const indexCompany = getIndex(idCompany, companies);
  if (indexCompany < 0) return 'not fount company';

  const indexUser = getIndex(idUser, companies[indexCompany].users);
  if (indexUser < 0) return 'not fount user';

  const user = companies[indexCompany].users[indexUser];

  companies[indexCompany].users.splice(indexUser, 1, {
    ...user,
    firstName: check(firstName, user.fistName),
    lastName: check(lastName, user.lastName),
    age: check(age, user.age),
    car: check(car, user.car),
    id: check(newId, idUser),
  });

  // console.log('companies : ', companies);
  return 'patch User';
}

function p8PutUser(idCompany, idUser, {firstName, lastName, car, age, newId}) {
  const indexCompany = getIndex(idCompany, companies);
  if (indexCompany < 0) return 'not fount company';

  const indexUser = getIndex(idUser, companies[indexCompany].users);
  if (indexUser < 0) return 'not fount user';

  const user = companies[indexCompany].users[indexUser];

  companies[indexCompany].users.splice(indexUser, 1, {
    ...user,
    firstName: check(firstName, ''),
    lastName: check(lastName, ''),
    age: check(age, NaN), // 🙄
    car: check(car, false), // 🙄
    id: check(newId, idUser), // 🙄
  });

  // console.log('companies : ', companies);
  return 'put User';
}

function p9MoveUser(idCompanyA, idCompanyB, idUser) {
  const indexCompanyA = getIndex(idCompanyA, companies);
  if (indexCompanyA < 0) return 'not fount company A';

  const indexCompanyB = getIndex(idCompanyB, companies);
  if (indexCompanyB < 0) return 'not fount company B';

  const indexUser = getIndex(idUser, companies[indexCompanyA].users);
  if (indexUser < 0) return 'not fount user';

  const user = companies[indexCompanyA].users[indexUser];

  companies[indexCompanyA].users.splice(indexUser, 1);
  companies[indexCompanyB].users.push(user);

  // console.log('companies : ', companies);
  return 'move User';
}

// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Parte 1: Crear una función tomando como parámetro un "id" de "company" y
// devolviendo el nombre de esta "company".

// Parte 2: Crear una función tomando como parámetro un "id" de "company" y
// quitando la "company" de la lista.

// Parte 3: Crear una función tomando como parámetro un "id" de "company" y
// permitiendo hacer un PATCH (como con una llamada HTTP) en todos los
// atributos de esta "company" excepto en el atributo "users".

// Parte 4: Crear una función tomando como parámetro un "id" de "company" y un
// nuevo "user" cuyo el apelido es "Delgado", el nombre "Juan", de 35 años y
// dueño de un carro. El nuevo "user" debe agregarse a la lista de "users" de este
// "company" y tener un "id" generado automáticamente. La función también debe modificar
// el atributo "usersLength" de "company".

// Parte 5: Crear una función tomando como parámetro un "id" de "company" y
// permitiendo hacer un PUT (como con una llamada HTTP) en esta "company" excepto
// en el atributo "users".

// Parte 6: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user". La función debe quitar este "user" de la lista de "users"
// de "company" y cambiar el atributo "usersLength" de "company".

// Parte 7: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user" que permite hacer un PATCH (como con una llamada HTTP) en este
// "user".

// Parte 8: Crear una función tomando como parámetro un "id" de "company" y un
// "id" de "user" que permite hacer un PUT (como con una llamada HTTP) en este
// "user".

// Parte 9: Crear una función tomando como parámetro dos "id" de "company" y
// un "id" de "user". La función debe permitir que el user sea transferido de la
// primera "company" a la segunda "company". El atributo "usersLength" de cada
// "company" debe actualizarse.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Part 1: Create a function taking as parameter an "id" of "company" and
// returning the name of this "company".

// Part 2: Create a function taking as parameter an "id" of "company" and
// removing the "company" from the list.

// Part 3: Create a function taking as a parameter an "id" of "company" and
// allowing to make a PATCH (as with an HTTP call) on all
// attributes of this "company" except on the "users" attribute.

// Part 4: Create a function taking as parameter an "id" of "company" and a
// new "user" whose name is "Delgado", the first name "Juan", aged 35 and
// a car. The new "user" must be added to the "users" list of this
// "company" and have an automatically generated "id". The function must also modify
// the "usersLength" attribute of "company".

// Part 5: Create a function taking as parameter an "id" of "company" and
// allowing to make a PUT (as with an HTTP call) on this "company" except
// on the "users" attribute.

// Part 6: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user". The function must remove this "user" from the list of "users"
// from "company" and change the attribute "usersLength" from "company".

// Part 7: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PATCH (as with an HTTP call) on this
// "user".

// Part 8: Create a function taking as a parameter an "id" of "company" and a
// "id" of "user" allowing to make a PUT (as with an HTTP call) on this
// "user".

// Part 9: Create a function taking as parameter two "id" of "company" and
// an "id" of "user". The function must allow the user to be transferred as a parameter
// from the 1st "company" to the 2nd "company". The "usersLength" attribute of each
// "company" must be updated.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Partie 1 : Créer une fonction prenant en paramètre un "id" de "company" et
// retournant le nom de cette "company".

// Partie 2 : Créer une fonction prenant en paramètre un "id" de "company" et
// supprimant la "company" de la liste.

// Partie 3 : Créer une fonction prenant en paramètre un "id" de "company" et
// permettant de faire un PATCH (comme avec un appel HTTP) sur tous les
// attributs de cette "company" sauf sur l'attribut "users".

// Partie 4 : Créer une fonction prenant en paramètre un "id" de "company" et un
// nouvel "user" dont le nom est "Delgado", le prénom "Juan", ayant 35 ans et
// une voiture. Le nouvel "user" doit être ajouté à la liste des "users" de cette
// "company" et avoir un "id" généré automatiquement. La fonction doit aussi modifier
// l'attribut "usersLength" de "company".

// Partie 5 : Créer une fonction prenant en paramètre un "id" de "company" et
// permettant de faire un PUT (comme avec un appel HTTP) sur cette "company" sauf
// sur l'attribut "users".

// Partie 6 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user". La fonction doit supprimer cet "user" de la liste des "users"
// de la "company" et modifier l'attribut "usersLength" de "company".

// Partie 7 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user" permettant de faire un PATCH (comme avec un appel HTTP) sur cet
// "user".

// Partie 8 : Créer une fonction prenant en paramètre un "id" de "company" et un
// "id" de "user" permettant de faire un PUT (comme avec un appel HTTP) sur cet
// "user".

// Partie 9 : Créer une fonction prenant en paramètre deux "id" de "company" et
// un "id" de "user". La fonction doit permettre de transférer l'user en paramètre
// de la 1re "company" à la 2e "company". L'attribut "usersLength" de chaque
// "company" doit être mis à jour.
