import {createAll, cleanConsole} from './data';
const companies = createAll();

cleanConsole(1, companies);
console.log('---- EXAMPLE 1 --- ', formatAndSortCompanies(companies));

function upperFirstLeter(srt) {
  return srt.charAt(0).toUpperCase()
      .concat(srt.slice(1));
}

function concatNames({firstName, lastName}) {
  return firstName.concat(lastName);
}

function formatAndSortUsers(users) {
  return users
      .map((user) => ({
        ...user,
        firstName: typeof user.firstName === 'undefined' ? '': upperFirstLeter(user.firstName),
        lastName: typeof user.lastName === 'undefined' ? '': upperFirstLeter(user.lastName),
      }))
      .sort((a, b) => concatNames(a).localeCompare(concatNames(b)))
  ;
}

export function formatAndSortCompanies(companies) {
  return companies
      .map((company) => ({
        ...company,
        users: formatAndSortUsers(company.users),
        name: upperFirstLeter(company.name),
      }))
      .sort((a, b) => b.users.length - a.users.length);
}


// -----------------------------------------------------------------------------
// INSTRUCCIONES EN ESPAÑOL

// Crear una función tomando la variable "companies" como parámetro y reemplazando
// todos los valores "undefined" en "usuarios" por un string vacío.
// El nombre de cada "company" debe tener una letra mayúscula al principio, así como
// el apellido y el nombre de cada "user".
// Las "companies" deben ordenarse por su total de "user" (orden decreciente)
// y los "users" de cada "company" deben aparecer en orden alfabético.

// -----------------------------------------------------------------------------
// INSTRUCTIONS IN ENGLISH

// Create a function taking the variable "companies" as a parameter and replacing
// all values "undefined" in "users" by an empty string.
// The name of each "company" must have a capital letter at the beginning as well as
// the last name and first name of each "user".
// The "companies" must be sorted by their number of "user" (decreasing order)
// and the "users" of each "company" must be listed in alphabetical order.

// -----------------------------------------------------------------------------
// INSTRUCTIONS EN FRANÇAIS

// Créer une fonction prenant en paramètre la variable "companies" et remplaçant
// toutes les valeurs "undefined" dans les "users" par un string vide.
// Le nom de chaque "company" doit avoir une majuscule au début ainsi que
// le nom et le prénom de chaque "user".
// Les "companies" doivent être triées par leur nombre de "user" (ordre décroissant)
// et les "users" de chaque "company" doivent être classés par ordre alphabétique.
